<?php

return [
    'gateway_task_url' => 'http://118.200.156.121/API/TaskHandle',
    'gateway_query_url' => 'http://118.200.156.121/API/QueryInfo',
    'gateway_user' => env('GATEWAY_USER'),
    'gateway_password' => env('GATEWAY_PASSWORD'),
];
