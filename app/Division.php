<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'division';

    public $timestamps = false;

    public $primaryKey = 'did';

    public static $status = [
        0 => '开启',
        1 => '关闭'
    ];

    public function getDstatusAttribute($value)
    {
        return self::$status[$value];
    }
}
