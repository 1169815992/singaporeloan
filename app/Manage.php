<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Manage extends Authenticatable
{
    use Notifiable;

    protected $table = 'manage';

    public $timestamps = false;

    const STATUS_0 = '正常';
    const STATUS_1 = '锁定';
    const STATUS_2 = '关闭';
    public static $status = [
        0 => self::STATUS_0,
        1 => self::STATUS_1,
        2 => self::STATUS_2
    ];

    public static $safe = [
        0 => '启用',
        1 => '关闭'
    ];

    public static $sex = [
        0 => '男',
        1 => '女'
    ];

    public function getGidAttribute($value)
    {
        return Group::select('gname')->find($value)->gname;
    }

    public function getDivisionAttribute($value)
    {
        return Division::select('dname')->find($value)->dname;
    }

    public function getStatusAttribute($value)
    {
        return self::$status[$value];
    }

    public function getSafeAttribute($value)
    {
        return self::$safe[$value];
    }

    public function getSearchingAttribute($value)
    {
        $searchArr = explode(',', $value);
        sort($searchArr);
        $searchArr = array_unique($searchArr);
        $searchArr = array_filter($searchArr);

        $divisonName = Division::select('dname')->whereIn('did', $searchArr)->get()->toArray();
        $divisonName = array_column($divisonName, 'dname');
        $divisonName = implode($divisonName, ',');

        return $divisonName;
    }

    public function getSexAttribute($value)
    {
        return self::$sex[$value];
    }
}
