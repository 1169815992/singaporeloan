<?php

namespace App\Http\Middleware;

use App\Operation;
use Closure;
use Illuminate\Support\Facades\Auth;

class OperationLog
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = 0;
        if (Auth::check()) {
            $user_id = Auth::id();
        }

        if ('GET' != $request->method()) {
            $operationModel = new Operation();
            $operationModel->uid = $user_id;
            $operationModel->path = $request->path();
            $operationModel->method = $request->method();
            $operationModel->ip = $request->ip();
            $operationModel->input = json_encode($request->all(), JSON_UNESCAPED_UNICODE);
            $operationModel->create = date('Y-m-d H:i:s');

            $operationModel->save();
        }

        return $next($request);
    }
}
