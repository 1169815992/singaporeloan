<?php

namespace App\Http\Middleware;

use App\Manage;
use Illuminate\Support\Facades\Auth;
use Closure;

class Authenticate
{

    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->status == Manage::STATUS_0) {
            return $next($request);
        }

        return redirect('/login');
    }
}
