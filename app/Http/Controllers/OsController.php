<?php

namespace App\Http\Controllers;

use App\Group;
use App\Operation;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OsController extends Controller
{
    public function roleList()
    {
        $groupSelected = Group::select('gid', 'gname')->where('gstatus', 0)->get();
        return view('os.role', [
            'group' => $groupSelected,
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('os.role_create');
        }

        if ($request->isMethod('post')) {
            $rules = [
                'gname' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            if ($request->exists('id')) {
                $groupModel = Group::find($request->get('id'));
            } else {
                $groupModel = new Group();
            }

            $groupModel->gname = $request->get('gname');
            $groupModel->gstatus = 0;

            if (!$groupModel->save()) {
                return back()->withErrors('保存失败');
            }

            return back();
        }
    }

    public function edit($id, Request $request)
    {
        if ($request->isMethod('get')) {
            $groupModel = Group::find($id);

            return view('os.role_edit', [
                'group' => $groupModel
            ]);
        }
    }

    public function loglist(Request $request)
    {
        $operationModel = new Operation();

        if ($request->exists('path') && !empty($request->get('path'))) {
            $operationModel = $operationModel->where('path', $request->get('path'));
        }

        if ($request->exists('start_time') && !empty($request->get('start_time'))) {
            $operationModel = $operationModel->where('create', '>=', $request->get('start_time'));
        }

        if ($request->exists('end_time') && !empty($request->get('end_time'))) {
            $operationModel = $operationModel->where('create', '<=', $request->get('end_time'));
        }

        $operationSelected = $operationModel->paginate(15);

        return view('os.log', [
            'operation' => $operationSelected
        ]);
    }

    public function smsSend(Request $request)
    {
        if ($request->isMethod('get')) {
            $ph = $request->get('ph') ?? '';
            return view('os.sms_send', [
                'ph' => $ph
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'content' => 'required|string',
                'phone' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $client = new Client([
                'auth' => [config('custom.gateway_user'), config('custom.gateway_password')]
            ]);

            $response = $client->request('POST', config('custom.gateway_task_url'), [
                'json' => [
                    'event' => 'txsms',
                    'userid' => Auth::id(),
                    'num' => $request->get('phone'),
                    'port' => '-1',
                    'encoding' => '8',
                    'smsinfo' => $request->get('content')
                ],
            ]);

            if ($response->getStatusCode() == 200) {
                $resCon = json_decode($response->getBody());

                if ($resCon->result == 'ok') {
                    return back();
                }

                return back()->withErrors($resCon->content);
            }


            return back()->withErrors('短信发送失败');
        }
    }

    public function smsList(Request $request)
    {
        $client = new Client(['auth' => [config('custom.gateway_user'), config('custom.gateway_password')]]);

        $parameter['event'] = 'queryrxsms';

        if ($request->exists('start_time') && !empty($request->get('start_time'))) {
            $parameter['begintime'] = date('YmdHis', strtotime($request->get('start_time')));
        } else {
            $parameter['begintime'] = date('YmdHis', strtotime("-1 week"));
        }

        if ($request->exists('end_time') && !empty($request->get('end_time'))) {
            $parameter['endtime'] = date('YmdHis', strtotime($request->get('end_time')));
        }

        if ($request->exists('phone') && !empty($request->get('phone'))) {
            $parameter['num'] = strtotime($request->get('phone'));
        }

        if ($request->exists('port') && !empty($request->get('port'))) {
            $parameter['port'] = strtotime($request->get('port'));
        }

        $response = $client->request('POST', config('custom.gateway_query_url'), [
            'json' => $parameter
        ]);

        if ($response->getStatusCode() == 200) {
            $resCon = $response->getBody()->getContents();
            $resCon = preg_replace("/\r\n/", '', $resCon);

            $resCon = json_decode($resCon);

            if ($resCon->result == 'ok') {
                $content = explode(';', $resCon->content);

                $content = array_map(function ($item) {
                    return explode(":", $item);
                }, $content);

                unset($content[0]);
                return view('os.sms', ['content' => $content]);
            }
            return back()->withErrors($resCon->content);
        }
        return back()->withErrors('短信获取失败');
    }

    //已经发送的短信
    public function smsSentList(Request $request)
    {
        $client = new Client(['auth' => [config('custom.gateway_user'), config('custom.gateway_password')]]);

        $parameter['event'] = 'querysxsms';

        if ($request->exists('start_time') && !empty($request->get('start_time'))) {
            $parameter['begintime'] = date('YmdHis', strtotime($request->get('start_time')));
        } else {
            $parameter['begintime'] = date('YmdHis', strtotime("-1 week"));
        }

        if ($request->exists('end_time') && !empty($request->get('end_time'))) {
            $parameter['endtime'] = date('YmdHis', strtotime($request->get('end_time')));
        }

        if ($request->exists('phone') && !empty($request->get('phone'))) {
            $parameter['num'] = strtotime($request->get('phone'));
        }

        if ($request->exists('port') && !empty($request->get('port'))) {
            $parameter['port'] = strtotime($request->get('port'));
        }

        $response = $client->request('POST', config('custom.gateway_query_url'), [
            'json' => $parameter
        ]);

        if ($response->getStatusCode() == 200) {
            $resCon = $response->getBody()->getContents();
            $resCon = preg_replace("/\"|}|\|E/", '', $resCon);
            $resCon = explode(';', $resCon);
            if (!empty($resCon)) {
                unset($resCon[0]);

                $resCon = array_map(function ($item) {
                    return explode(":", $item);
                }, $resCon);

                return view('os.sms_sent', ['content' => $resCon]);
            }
            return back()->withErrors($resCon->content);
        }
        return back()->withErrors('短信获取失败');
    }
}
