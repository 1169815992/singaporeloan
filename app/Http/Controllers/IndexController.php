<?php

namespace App\Http\Controllers;

use App\Division;
use App\Http\Controllers\help\FreeApiController;
use App\Http\Controllers\help\getMacController;
use App\Manage;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Agent\Facades\Agent;

class IndexController extends Controller
{
    public function index()
    {
        $userCount = User::count();
        $userCountToday = User::where('createdate', date('Y-m-d H:i:s', time()))->count();
        $staffCount = Manage::count();
        $branchCount = Division::count();

        //最近一月数据涨幅
        $userOneMonth = User::select('uid', 'createdate')
            ->where('createdate', '>', date('Y-m-d', strtotime("-1 month")))
            ->where('createdate', '<', date('Y-m-d', strtotime("1 day")))
            ->orderBy('createdate', 'desc')
            ->get();

        //设定的日期
        $dateStan = [];
        $startJ = strtotime("-1 month");
        for ($i = 0; $i < 31; $i++) {
            $dateStan[] = date('j', $startJ);
            $startJ += 86400;
        }

        //实际的数据
        $userOneMonthOpt = [];
        foreach ($userOneMonth as $item) {
            $date = (int)date('j', strtotime($item->createdate));
            if (isset($userOneMonthOpt[$date])) {
                $userOneMonthOpt[$date]++;
            } else {
                $userOneMonthOpt[$date] = 1;
            }
        }

        //相互组合
        $userOneMonth = [];
        foreach ($dateStan as $item) {
            if (isset($userOneMonthOpt[$item])) {
                $userOneMonth[] = [$item, $userOneMonthOpt[$item]];
            } else {
                $userOneMonth[] = [$item, 0];
            }
        }

        return view('index', [
            'userCount' => $userCount,
            'userCountToday' => $userCountToday,
            'staffCount' => $staffCount,
            'branchCount' => $branchCount,
            'userOneMonth' => json_encode($userOneMonth),
            'abscissa' => json_encode($dateStan),
        ]);
    }

    public function login(Request $request)
    {
        if ($request->isMethod('get')) {

            $mac = '';
            if ($request->exists('cpm')) {
                $mac = base64_decode($request->get('cpm'));
                $mac = str_replace(".20E.LR.", "-", $mac);
            }

            return view('user.login', [
                'mac' => $mac
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'loginname' => 'required|string',
                'loginpass' => 'required|string'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $name = $request->get('loginname');
            $pass = md5($request->get('loginpass'));

            $auth = Manage::select('id', 'loginname', 'ename', 'gid', 'status', 'safemac', 'division')
                ->where('loginname', '=', $name)
                ->where('loginpass', '=', $pass)
                ->where('status', 0)
                ->first();

            if (!$auth) {
                return back();
            }


            if ($auth->gid != '管理员') {
                //如果没有就绑定
                if (!$auth->safemac) {
                    $auth->safemac = $request->get('mac');
                    $auth->save();
                }

                //判断mac
                if ($auth->safemac != $request->get('mac')) {
                    return back();
                }
            }

            //查询是否超出登陆范围
            $division = Division::select('dip', 'xpos', 'ypos')->where('dname', $auth->division)->first();
            $client = new Client();

            //获取分部位置
            $pos = $client->request('get', "http://ip-api.com/json/{$division->dip}?lang=zh-CN");
            $divisionPos = json_decode($pos->getBody());

            $pos = $client->request('get', "http://ip-api.com/json?lang=zh-CN");
            $clientPos = json_decode($pos->getBody());

            if ($auth->gid != '管理员' && ($divisionPos->lat - $clientPos->lat) * 111000 > $division->xpos) {
                return back()->withErrors('你所在地区不允许登陆');
            }

            Auth::login($auth);
            return redirect('/');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function account(Request $request)
    {
        if ($request->isMethod('get')) {
            $manageSelected = Manage::find(Auth::id());

            return view('user.account', [
                'account' => $manageSelected
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'loginname' => 'required|string',
                'ename' => 'required|string',
                'id' => 'required|numeric'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $manageModel = Manage::select('id', 'loginname', 'loginpass', 'ename')->find($request->get('id'));
            $manageModel->loginname = $request->get('loginname');
            $manageModel->ename = $request->get('ename');

            if ($request->exists('loginpass') && $request->get('loginpass') != '') {
                $manageModel->loginpass = md5($request->get('loginpass'));
            }

            if ($manageModel->save()) {
                Auth::logout();
                return redirect('/login');
            }

            return back()->withErrors('网络异常');
        }
    }
}
