<?php

namespace App\Http\Controllers;

use App\Division;
use App\Group;
use App\Manage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    public function divisionList()
    {
        $divisionSelected = Division::select('did', 'dname', 'dstime', 'detime', 'dstatus', 'dip')
            ->orderBy('dname', 'asc')
            ->paginate(20);

        return view('staff.division', [
            'division' => $divisionSelected
        ]);
    }

    public function divisionCreate(Request $request)
    {
        if ($request->isMethod('get')) {
            $manageSelected = Manage::select('id', 'loginname')->where('status', 0)->orderBy('division', 'asc')->get();
            return view('staff.division_create', [
                'staff' => $manageSelected
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'name' => 'required|string',
                'time' => 'required|string',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $time = explode('-', $request->get('time'));

            try {
                if ($request->exists('id')) {
                    $divisionModel = Division::find($request->get('id'));
                } else {
                    $divisionModel = new Division();
                }

                $divisionModel->dname = $request->get('name');
                $divisionModel->dstime = trim($time[0]);
                $divisionModel->detime = $time[1];
                $divisionModel->dip = $request->get('dip');
                $divisionModel->xpos = $request->get('xpos');
                $divisionModel->ypos = $request->get('ypos');

                if ($request->exists('status')) {
                    $divisionModel->dstatus = $request->get('status');
                }

                if (!$divisionModel->save()) {
                    return back()->withErrors('数据插入异常');
                }

                if ($request->exists('staff')) {
                    $manages = $request->get('staff');
                    if (!empty($manages)) {
                        sort($manages);
                        $manages = array_unique($manages);

                        $res = Manage::whereIn('id', $manages)->update(['searching' => DB::raw('concat(searching,' . "',{$divisionModel->id}')")]);

                        if (!$res) {
                            return back()->withErrors('数据更新异常');
                        }
                    }
                }

            } catch (\Exception $e) {
                return back()->withErrors('网络异常');
            }

            return back();
        }
    }

    public function divisionEdit($id, Request $request)
    {
        if ($request->isMethod('get')) {
            $divisionSelected = Division::find($id);

            //$manageSelected = Manage::select('id', 'loginname')->where('status', 0)->orderBy('division', 'asc')->get();
            return view('staff.division_edit', [
                'division' => $divisionSelected,
                'status' => Division::$status
                //'manage' => $manageSelected
            ]);
        }
    }

    public function staffList()
    {
        $manageSelected = Manage::orderBy('gid', 'asc')->orderBy('loginname', 'asc')->paginate(20);

        return view('staff.staff', [
            'manage' => $manageSelected
        ]);
    }

    public function staffCreate(Request $request)
    {
        if ($request->isMethod('get')) {
            $sex = Manage::$sex;
            $division = Division::select('did', 'dname')->where('dstatus', 0)->get();
            $status = Manage::$status;
            $safe = Manage::$safe;
            $group = Group::select('gid', 'gname')->get();

            return view('staff.create', [
                'sex' => $sex,
                'division' => $division,
                'status' => $status,
                'safe' => $safe,
                'group' => $group,
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'loginname' => 'required|string',
                'ename' => 'required|string',
                'division' => 'required|string',
                'group' => 'required|string',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            if ($request->exists('searching')) {
                $searching = implode(',', $request->get('searching'));
            }

            if ($request->get('id')) {
                $manageModel = Manage::find($request->get('id'));
            } else {
                $manageModel = new Manage();
            }

            $manageModel->loginname = $request->get('loginname');
            if (!empty($request->get('loginpass'))) {
                $manageModel->loginpass = md5($request->get('loginpass'));
            }
            $manageModel->ename = $request->get('ename');
            $manageModel->sex = $request->get('sex');
            $manageModel->division = $request->get('division');
            $manageModel->gid = $request->get('group');
            $manageModel->searching = $searching;
            $manageModel->status = $request->get('status');
            $manageModel->safe = $request->get('safe');
            $manageModel->safemac = $request->get('mac');
            $manageModel->ip = $request->getClientIp();
            $manageModel->createdate = date('Y-m-d H:i:s');

            if ($manageModel->save()) {
                return back();
            }
        }
    }

    public function staffEdit($id, Request $request)
    {
        if ($request->isMethod('get')) {
            $sex = Manage::$sex;
            $division = Division::select('did', 'dname')->where('dstatus', 0)->get();
            $status = Manage::$status;
            $safe = Manage::$safe;
            $group = Group::select('gid', 'gname')->get();

            $manageSelect = Manage::find($id);

            return view('staff.edit', [
                'sex' => $sex,
                'division' => $division,
                'status' => $status,
                'safe' => $safe,
                'group' => $group,
                'manage' => $manageSelect
            ]);
        }
    }


    public function detail($id)
    {
        $manageSelected = Manage::find($id);

        return view('staff.detail', [
            'manage' => $manageSelected
        ]);
    }

    //删除
    public function del($id)
    {
        if (Manage::destroy($id)) {
            return 200;
        }
    }
}
