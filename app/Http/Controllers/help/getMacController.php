<?php

namespace App\Http\Controllers\help;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class getMacController extends Controller
{
    private $result = [];
    private $macAddr;

    function GetMac($OS){
        switch ( strtolower($OS) ){
            case "unix": break;
            case "solaris": break;
            case "aix": break;
            case "linux":
                $this->getLinux();
                break;
            default:
                $this->getWindows();
                break;
        }

        $tem = array();
        foreach($this->result as $val){
            if(preg_match("/[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f][:-]"."[0-9a-f][0-9a-f]/i",$val,$tem) )
            {
                $this->macAddr = $tem[0];//多个网卡时，会返回第一个网卡的mac地址
                break;
            }
        }
        return $this->macAddr;
    }
    //Linux系统
    function getLinux(){
        @exec("ifconfig -a", $this->result);
        return $this->result;
    }
    //Windows系统
    function getWindows(){
        @exec("ipconfig /all", $this->result);
        if ( $this->result ) {
            return $this->result;
        } else {
            $ipconfig = $_SERVER["windir"]."\system32\ipconfig.exe";
            if(is_file($ipconfig)) {
                @exec($ipconfig." /all", $this->result);
            } else {
                @exec($_SERVER["windir"]."\system\ipconfig.exe /all", $this->result);
                return $this->result;
            }
        }
    }
}
