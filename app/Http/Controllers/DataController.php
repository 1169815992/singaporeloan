<?php

namespace App\Http\Controllers;

use App\Division;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DataController extends Controller
{
    public function list(Request $request)
    {
        $userData = User::select('uid', 'IC', 'department', 'Phone', 'LoanStatus', 'LoansCause', 'creator', 'createdate')
            ->orderBy('uid', 'desc');

        if ($request->exists('search') && $request->get('search') != '') {
            $userData = $userData
                ->where('IC', $request->get('search'))
                ->orwhere('Phone', $request->get('search'))
                ->orwhere('Phone1', $request->get('search'))
                ->orwhere('Phone2', $request->get('search'))
                ->orwhere('zip', $request->get('search'))
                ->orwhere('CompanyZip', $request->get('search'))
                ->orwhere('rsphone1', $request->get('search'))
                ->orwhere('rsphone2', $request->get('search'))
                ->orwhere('rsphone3', $request->get('search'))
                ->orwhere('rsphone4', $request->get('search'))
                ->orwhere('rsphone5', $request->get('search'))
                ->orwhere('rsphone6', $request->get('search'));
        }

        $userData = $userData->paginate(20);

        return view('data.data', [
            'user' => $userData,
            'search' => $request->get('search') ?? ''
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('get')) {
            //depart info
            $divisionData = Division::select('did', 'dname')->where('dstatus', '=', 0)->get();
            //贷款状态
            $loanStatus = User::$loanStatus;

            return view('data.create', [
                'division' => $divisionData,
                'loanStatus' => $loanStatus
            ]);
        }

        if ($request->isMethod('post')) {
            $rules = [
                'ic' => 'required|string',
                'ph1' => 'required|string',
                'division' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            //文件上传
            if (!empty($request->file('agreement'))) {
                $path = $request->file('agreement')->store('agreement');
            }

            if ($request->exists('id')) {
                $userModel = User::find($request->get('id'));
            } else {
                $userModel = new User();
            }

            $userModel->IC = $request->get('ic');
            $userModel->department = $request->get('division');
            $userModel->Phone = $request->get('ph1');
            $userModel->Phone1 = $request->get('ph2');
            $userModel->Phone2 = $request->get('ph3');
            $userModel->LoanStatus = $request->get('loan_cause');
            $userModel->LoansCause = $request->get('loan_reason');
            $userModel->Notes = $request->get('remarks');
            $userModel->tel = $request->get('home_no');
            $userModel->Address = $request->get('home_address');
            $userModel->zip = $request->get('code');
            $userModel->CompanyName = $request->get('campany');
            $userModel->CompanyPhone = $request->get('office_no');
            $userModel->Position = $request->get('position');
            $userModel->Salary = $request->get('salary_data');
            $userModel->WorkingAge = $request->get('loe');
            $userModel->CompanyAddress = $request->get('office_add');
            $userModel->CompanyZip = $request->get('company_code');
            $userModel->createdate = date('Y-m-d H:i:s');
            $userModel->creator = Auth::id();
            $userModel->rsname1 = $request->get('rela_1_name');
            $userModel->rid1 = $request->get('rela_1_relation');
            $userModel->rsphone1 = $request->get('rela_1_ph');
            $userModel->rsname2 = $request->get('rela_2_name');
            $userModel->rid2 = $request->get('rela_2_relation');
            $userModel->rsphone2 = $request->get('rela_2_ph');
            $userModel->rsname3 = $request->get('rela_3_name');
            $userModel->rid3 = $request->get('rela_3_relation');
            $userModel->rsphone3 = $request->get('rela_3_ph');
            $userModel->rsname4 = $request->get('rela_4_name');
            $userModel->rid4 = $request->get('rela_4_relation');
            $userModel->rsphone4 = $request->get('rela_4_ph');
            $userModel->rsname5 = $request->get('rela_5_name');
            $userModel->rid5 = $request->get('rela_5_relation');
            $userModel->rsphone5 = $request->get('rela_5_ph');
            $userModel->rsname6 = $request->get('rela_6_name');
            $userModel->rid6 = $request->get('rela_6_relation');
            $userModel->rsphone6 = $request->get('rela_6_ph');

            if (!empty($request->file('agreement'))) {
                $userModel->agreement = $path;
            }

            if ($request->exists('id')) {
                $userModel->updatedate = date('Y-m-d H:i:s');
            }

            if ($userModel->save()) {
                return back();
            }

            return back()->with('errors', 'Have an error');
        }
    }

    public function edit($id, Request $request)
    {
        if ($request->isMethod('get')) {
            $user = User::find($id);
            $loanStatus = User::$loanStatus;

            $divisonSelected = Division::select('did', 'dname')->get();

            return view('data.edit', [
                'user' => $user,
                'loanStatus' => $loanStatus,
                'division' => $divisonSelected
            ]);
        }
    }

    public function detail($id)
    {
        $user = User::find($id);
        $loanStatus = User::$loanStatus;

        return view('data.detail', [
            'user' => $user,
            'loanStatus' => $loanStatus
        ]);
    }

    //下载合同
    public function downContract($id)
    {
        $agreement = User::select('IC', 'agreement')->find($id);
        if ($agreement->agreement) {
            $file = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $agreement->agreement;
            $file_ext = explode('.', $file);
            $name = $agreement->IC . '_contract_.' . $file_ext[count($file_ext) - 1];

            return response()->download($file, $name);
        }

        return back();
    }

    //删除
    public function del($id)
    {
        if (User::destroy($id)) {
            return 200;
        }
    }
}
