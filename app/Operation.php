<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    public $timestamps = false;
    public $table = 'operation_log';

    public function getUidAttribute($value)
    {
        if ($value) {
            return Manage::select('loginname')->find($value)->loginname;
        }

        return '--';
    }
}
