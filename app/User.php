<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    static public $loanStatus = [
        0 => 'YES(已获批)',
        1 => 'NO(未获批)'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    //时间格式化输出
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public $primaryKey = 'uid';

    public $timestamps = false;

    public function getLoanStatusAttribute($value)
    {
        return self::$loanStatus[(int)$value];
    }

    public function getDepartmentAttribute($value)
    {
        $departments = Division::select('did', 'dname')->get()->toArray();

        $departments = array_column($departments, null, 'did');

        return $departments[$value]['dname'] ?? '';
    }

    public function getCreatorAttribute($value)
    {
        $loginname = Manage::select('loginname')->find($value);

        if (empty($loginname)) {
            return '--';

        }

        return $loginname->loginname;
    }
}
