<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $table = 'group';
    public $primaryKey = 'gid';
    public $timestamps = false;

    public static $status = [
        0 => '正常',
        1 => '关闭'
    ];

    public function getGstatusAttribute($value)
    {
        return self::$status[(int)$value];
    }

}
