<?php
Route::match(['get', 'post'], '/login', 'IndexController@login')->name('login');
Route::get('/logout', 'IndexController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/search', 'IndexController@search');
    Route::match(['get', 'post'], '/account', 'IndexController@account');

    Route::get('/data', 'DataController@list');
    Route::match(['get', 'post'], 'data/create', 'DataController@create');
    Route::get('/data/{id}/edit', 'DataController@edit');
    Route::get('/data/{id}/detail', 'DataController@detail');
    Route::get('/data/{id}/del', 'DataController@del');
    Route::get('/data/{id}/contract', 'DataController@downContract');

    Route::get('/staff/division', 'StaffController@divisionList');
    Route::match(['get', 'post'], '/staff/division/create', 'StaffController@divisionCreate');
    Route::match(['get', 'post'], '/staff/division/{id}/edit', 'StaffController@divisionEdit');

    Route::get('/staff', 'StaffController@staffList');
    Route::match(['get', 'post'], '/staff/create', 'StaffController@staffCreate');
    Route::get('/staff/{id}/edit', 'StaffController@staffEdit');
    Route::get('/staff/{id}/detail', 'StaffController@detail');
    Route::get('/staff/{id}/del', 'StaffController@del');

    Route::get('/os/role', 'OsController@roleList');
    Route::match(['get', 'post'], '/os/role/create', 'OsController@create');
    Route::get('/os/role/{id}/edit', 'OsController@edit');

    Route::get('/os/log', 'OsController@loglist');

    Route::match(['get', 'post'], '/sms/send', 'OsController@smsSend');
    Route::get('/sms', 'OsController@smsList');
    Route::get('/sms/sent', 'OsController@smsSentList');
});
