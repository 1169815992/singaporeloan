<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> Login </title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>

<body style="background-color: #364150">

<div class="text-center animated fadeInDown"
     style="background-color: #ffffff;padding: 30px;width: 25%;margin: 10vw auto;font-size: 16px;">
    <div>
        <h3 style="font-size: 30px;padding-bottom: 20px;">登陆</h3>
        <form role="form" action="/login" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="mac" value="{{ $mac }}"/>
            <div class="form-group">
                <input type="text" name="loginname" class="form-control" placeholder="登陆用户名" required autocomplete="off"
                       style="padding: 15px;">
            </div>
            <div class="form-group">
                <input type="password" name="loginpass" class="form-control" placeholder="登陆用户密码" required
                       autocomplete="off" style="padding: 15px;">
            </div>
            <button type="submit" class="btn btn-info btn-block" style="padding: 15px;">Login</button>
        </form>
    </div>
</div>
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/bootstrap.js"></script>
</body>
</html>
