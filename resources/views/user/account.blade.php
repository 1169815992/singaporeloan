@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Personal center <small> 个人中心 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Personal center 个人中心</strong>
                </li>
            </ol>
        </div>
    </div>

    <style type="text/css">
        .media-body {
            padding: 10px 0;
        }
    </style>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-9 ibox">
                <div class="ibox-content">
                    <div class="chat-activity-list">
                        <form action="/account" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $account->id }}"/>
                            <div class="media-body ">
                                <strong> 登录账号 </strong>
                                <input type="text" class="form-control" name="loginname"
                                       value="{{ $account->loginname }}">
                            </div>
                            <div class="media-body ">
                                <strong> 登录密码 （留空即为保持原来的密码）</strong>
                                <input type="text" class="form-control" name="loginpass" value="">
                            </div>
                            <div class="media-body ">
                                <strong> 显示昵称 </strong>
                                <input type="text" class="form-control" name="ename" value="{{ $account->ename }}">
                            </div>
                            <div class="media-body ">
                                <input type="submit" class="btn btn-info" value="submit">
                            </div>
                        </form>
                    </div>
                </div>
                <script>
                    var error = "{{ $errors->count() }}";
                    if (error != 0) {
                        alert('{{ $errors->first() }}');
                    }
                </script>
            </div>
        </div>
    </div>
@endsection
