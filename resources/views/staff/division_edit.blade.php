@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data addition <small> 数据新增 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/staff/division"><strong>division list 分部列表</strong> </a>
                </li>
                <li class="breadcrumb-item active">
                    Division edit 编辑分部
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <h5>Division edit <small>编辑分部</small></h5>
                </div>
                <div class="ibox-content">
                    <form action="/staff/division/create" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $division->did }}">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> name（分部名称） * </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="name"
                                                          value="{{ $division->dname }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> time（工作时间） * </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="time" id="time" required
                                       autocomplete="off"
                                       value="{{ $division->dstime }} - {{ $division->detime }}">
                            </div>
                            <script>
                                laydate.render({
                                    elem: '#time',
                                    type: 'time',
                                    range: true,
                                    format: 'HH:mm',
                                });
                            </script>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> status（分部状态）</label>
                            <div class="col-sm-10">
                                @foreach($status as $k=>$item)
                                    <label id="status{{ $k }}">
                                        <input type="radio" name="status" id="status{{ $k }}"
                                               value="{{ $k }}" @if($division->dstatus == $item) checked @endif>
                                        &nbsp;{{ $item }} &nbsp;
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> ip（网络ip）</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="dip" value="{{ $division->dip }}"
                                       placeholder="填写当前分部所处的网络ip地址">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 横纵向扩展距离</label>
                            <div class="col-sm-5"><input type="number" class="form-control" name="xpos"
                                                         value="{{ $division->xpos }}"
                                                         placeholder="允许东西扩展距离（m）">
                            </div>
                            <div class="col-sm-5"><input type="number" class="form-control" name="ypos"
                                                         value="{{ $division->ypos }}"
                                                         placeholder="允许南北扩展距离（m）">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> save</button>
                                <a href="/staff/division" class="btn btn-white btn"> cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

