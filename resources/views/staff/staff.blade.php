@extends('basic')

@section('content')
    <div id="modal-form" class="modal" style="background: rgba(0,0,0,0.4)">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 b-r">
                            <h3>You want to delete it? </h3>
                            <br>
                            <div>
                                <button class="btn btn-block btn-primary" type="submit" id="remove_del">
                                    <strong>No</strong></button>
                                <button id="action_del" class="btn btn-block" type="submit">yes
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-center">
                                <i class="fa fa-trash-o big-icon"></i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Staff list 员工列表</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <div class="col-sm-2">
                        <a href="/staff/create" class="btn btn-info"> <i class="fa fa-plus"></i> 添加员工</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive text-center">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>account (员工登陆名)</th>
                                <th>name (员工姓名)</th>
                                <th>role (用户组)</th>
                                <th>branch (所属分部)</th>
                                <th>MAC (绑定MAC)</th>
                                <th>status (用户状态)</th>
                                <th>可搜索分部</th>
                                <th>Operation (操作)</th>
                            </tr>
                            @foreach($manage as $item)
                                <tr>
                                    <td>{{ $item->loginname }}</td>
                                    <td>{{ $item->ename }}</td>
                                    <td>{{ $item->gid }}</td>
                                    <td>{{ $item->division }}</td>
                                    <td>{{ $item->safemac }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>{{ $item->searching }}</td>
                                    <td>
                                        <a href="/staff/{{$item->id}}/detail" class="btn btn-white btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="/staff/{{$item->id}}/edit" class="btn btn-white btn-sm">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @if(Auth::user()->gid == '管理员')
                                            <button class="btn btn-white btn-sm" onclick="del_item({{ $item->id }})">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {!! $manage->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function del_item(id) {
            $("#modal-form").fadeIn();
            $("#action_del").click(function () {
                $.get("/staff/" + id + "/del", function (data) {
                    if (data == 200) {
                        location.reload();
                    }
                });
            });
            $("#remove_del").click(function () {
                $("#modal-form").fadeOut();
            });
        }
    </script>
@endsection

