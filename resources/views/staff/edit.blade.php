@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data addition <small> 数据新增 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/staff"><strong>staff list 员工列表</strong></a>
                </li>
                <li class="breadcrumb-item active">
                    edit staff 修改信息修改
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-9 ibox">
                <div class="ibox-title">
                    <h5>edit staff <small>修改信息修改</small></h5>
                </div>
                <div class="ibox-content">
                    <form action="/staff/create" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $manage->id }}">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 登陆名 </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="loginname"
                                                          value="{{ $manage->loginname }}" required></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 登陆密码 </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="loginpass"
                                                          placeholder="留空为默认密码"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 员工名 </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="ename"
                                                          value="{{ $manage->ename }}" required></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 性别 </label>
                            <div class="col-sm-10">
                                @foreach($sex as $k => $item)
                                    <label id="sex{{$k}}">
                                        <input type="radio" name="sex" id="sex{{$k}}"
                                               value="{{$k}}" {{ $item==$manage->sex?'checked':'' }}>
                                        &nbsp;{{ $item }} &nbsp;
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 所属分部 </label>
                            <div class="col-sm-10">
                                <select name="division" class="form-control">
                                    @foreach($division as $item)
                                        <option
                                            value="{{ $item->did }}" {{ $item->dname==$manage->division?'selected':'' }}> {{ $item->dname }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 所属组 </label>
                            <div class="col-sm-10">
                                <select name="group" class="form-control">
                                    @foreach($group as $item)
                                        <option
                                            value="{{ $item->gid }}" {{ $item->gname==$manage->gid?'selected':'' }}> {{ $item->gname }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 可搜索分部 </label>
                            <div class="col-sm-10">
                                @foreach($division as $item)
                                    <label id="searching{{ $item->did }}" style="padding-right: 12px;">
                                        <input type="checkbox" name="searching[]" id="searching{{ $item->did }}"
                                               value="{{ $item->did }}" {{ strpos($manage->searching,$item->dname) !== false?'checked':'' }}>
                                        &nbsp;{{ $item->dname }}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 状态 </label>
                            <div class="col-sm-10">
                                @foreach($status as $k => $item)
                                    <label id="status{{$k}}">
                                        <input type="radio" name="status" id="status{{$k}}"
                                               value="{{$k}}" {{ $item ==$manage->status?'checked':'' }}>
                                        &nbsp;{{ $item }} &nbsp;
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 安全设置 </label>
                            <div class="col-sm-10">
                                @foreach($safe as $k => $item)
                                    <label id="safe{{$k}}">
                                        <input type="radio" name="safe" id="safe{{$k}}"
                                               value="{{$k}}" {{ $item==$manage->safe?'checked':'' }}>
                                        &nbsp;{{ $item }} &nbsp;
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 用户MAC </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="mac" value="{{ $manage->safemac }}">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-info btn" type="submit"> 重新绑定</button>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> save</button>
                                <a href="/staff" class="btn btn-white btn"> cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

