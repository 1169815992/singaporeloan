@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data addition <small> 数据新增 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="/staff/division"><strong>division list 分部列表</strong> </a>
                </li>
                <li class="breadcrumb-item active">
                    Division addition 添加分部
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <h5>Division addition <small>添加分部</small></h5>
                </div>
                <div class="ibox-content">
                    <form action="/staff/division/create" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> name（分部名称） * </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="name" required></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> time（工作时间） * </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="time" id="time" required
                                       autocomplete="off">
                            </div>
                            <script>
                                laydate.render({
                                    elem: '#time',
                                    type: 'time',
                                    range: true,
                                    format: 'HH-mm',
                                });
                            </script>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> staff（选择可搜索） * </label>
                            <div class="col-sm-10">
                                @foreach($staff as $item)
                                    <label id="department{{ $item->id }}" style="width: 15%">
                                        <input type="checkbox" name="staff[]" id="staff{{ $item->id }}"
                                               value="{{ $item->id }}" checked> &nbsp;{{ $item->loginname }} &nbsp;
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> ip（网络ip）</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="dip"
                                                          placeholder="填写当前分部所处的网络ip地址">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 横纵向扩展距离</label>
                            <div class="col-sm-5"><input type="number" class="form-control" name="xpos"
                                                         placeholder="允许东西扩展距离（m）">
                            </div>
                            <div class="col-sm-5"><input type="number" class="form-control" name="ypos"
                                                         placeholder="允许南北扩展距离（m）">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> save</button>
                                <a href="/staff/division" class="btn btn-white btn"> cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

