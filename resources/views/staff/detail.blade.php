@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/staff"><strong>Staff list 员工列表</strong></a>
                </li>
                <li class="breadcrumb-item active">
                    See details 查看详情
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    ID : {{ $manage->id }}
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th width="15%">属性名</th>
                            <th>属性值</th>
                            <th width="15%">属性名</th>
                            <th>属性值</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> 登陆用户名 </td>
                            <td>{{ $manage->loginname }}</td>
                            <td> 用户名 </td>
                            <td>{{ $manage->ename }}</td>
                        </tr>
                        <tr>
                            <td> 性别</td>
                            <td>{{ $manage->sex }}</td>
                            <td> 所在分部</td>
                            <td>{{ $manage->division }}</td>
                        </tr>
                        <tr>
                            <td> 所属组 </td>
                            <td>{{ $manage->gid }}</td>
                            <td> 可以搜索分部 </td>
                            <td>{{ $manage->searching }}</td>
                        </tr>
                        <tr>
                            <td> 状态 </td>
                            <td>{{ $manage->status }}</td>
                            <td> 安全状态 </td>
                            <td>{{ $manage->safe }}</td>
                        </tr>
                        <tr>
                            <td> MAX </td>
                            <td>{{ $manage->safemac }}</td>
                            <td> ip </td>
                            <td>{{ $manage->ip }}</td>
                        </tr>
                        <tr>
                            <td> 登陆ip </td>
                            <td>{{ $manage->loginip }}</td>
                            <td> 创建时间 </td>
                            <td>{{ $manage->createdate }}</td>
                        </tr>
                        <tr>
                            <td> 最后一次修改时间 </td>
                            <td>{{ $manage->changedate }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

