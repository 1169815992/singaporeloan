@extends('basic')

@section('content')
    <div id="modal-form" class="modal" style="background: rgba(0,0,0,0.4)">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 b-r">
                            <h3>You want to delete it? </h3>
                            <br>
                            <div>
                                <button class="btn btn-block btn-primary" type="submit" id="remove_del">
                                    <strong>No</strong></button>
                                <button id="action_del" class="btn btn-block" type="submit">yes
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-center">
                                <i class="fa fa-trash-o big-icon"></i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Data list 数据列表</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <form action="/data" type="get">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="search" placeholder="Search by IC or Phone or company name or Postal Code"
                                       value="{{ $search }}" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive text-center">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>ID(编号)</th>
                                <th>COUNTER(所属分部)</th>
                                <th>IC (身份证)</th>
                                <th>HP (手机)</th>
                                <th>APPROVED? (是否批款)</th>
                                <th>REASON IF NO (未批原因)</th>
                                <th>CREATOR (创建者)</th>
                                <th>Entry Time (录入时间)</th>
                                <th>Operation (操作)</th>
                            </tr>

                            @foreach($user as $item)
                                <tr>
                                    <td>{{ $item->uid }}</td>
                                    <td>{{ $item->department }}</td>
                                    <td>
                                        <span
                                            class="{{ $item->IC==$search?'font-bold text-success':'' }}">{{ $item->IC }}</span>
                                    </td>
                                    <td>
                                        <span class="{{ $item->Phone==$search?'font-bold text-success':'' }}">
                                            {{ Auth::user()->gid == '员工' && !empty(trim($item->Phone))?substr_replace($item->Phone,'**',3,2):$item->Phone }}
                                        </span>
                                    </td>
                                    <td>{{ $item->LoanStatus }}</td>
                                    <td width="40%">{{ $item->LoansCause }}</td>
                                    <td>{{ $item->creator }}</td>
                                    <td>{{ $item->createdate }}</td>
                                    <td>
                                        <a href="/data/{{ $item->uid }}/detail" class="btn btn-white btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        @if(Auth::user()->gid == '员工' && $item->createdate >= date('Y-m-d'))
                                            <a href="/data/{{ $item->uid }}/edit" class="btn btn-white btn-sm">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endif
                                        @if(Auth::user()->gid != '员工')
                                            <a href="/data/{{ $item->uid }}/edit" class="btn btn-white btn-sm">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endif

                                        {{-- @if(Auth::user()->gid == '管理员')
                                             <button class="btn btn-white btn-sm" onclick="del_item({{ $item->uid }})">
                                                 <i class="fa fa-trash-o"></i>
                                             </button>
                                         @endif--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {!! $user->links() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        function del_item(id) {
            $("#modal-form").fadeIn();
            $("#action_del").click(function () {
                $.get("/data/" + id + "/del", function (data) {
                    if (data == 200) {
                        location.reload();
                    }
                });
            });
            $("#remove_del").click(function () {
                $("#modal-form").fadeOut();
            });
        }
    </script>
@endsection

