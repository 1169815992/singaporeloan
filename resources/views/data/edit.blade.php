@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/data"><strong>Data list 数据列表</strong></a>
                </li>
                <li class="breadcrumb-item active">
                    Edit 编辑
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    ID : {{ $user->uid }}
                    <div class="ibox-tools">
                        <a href="/data/{{ $user->uid }}/contract"> <span class="label label-primary">Download contract scan（下载合同扫描件） </span></a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="/data/create" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $user->uid }}"/>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> IC(身份证号) * </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="ic"
                                                          value="{{ $user->IC }}" required readonly></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> COUNTER(所属分部) * </label>
                            <div class="col-sm-10">
                                <select name="division" class="form-control">
                                    @foreach($division as $item)
                                        <option
                                            value="{{ $item->did }}" {{ $item->dname == $user->department?'selected':'' }}>
                                            {{ $item->dname }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> IC(合同上传) </label>
                            <div class="col-sm-10">
                                <div class="custom-file">
                                    <input id="logo" type="file" name="agreement" class="custom-file-input">
                                    <label for="logo" class="custom-file-label">Choose file...</label>
                                </div>
                                <script>
                                    $('.custom-file-input').on('change', function () {
                                        let fileName = $(this).val().split('\\').pop();
                                        $(this).next('.custom-file-label').addClass("selected").html(fileName);
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                HP 1(手机号1) *
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="ph1"
                                                          value="{{ $user->Phone }}" required></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                HP 2(手机号2)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="ph2"
                                                          value="{{ $user->Phone1 }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                HP 3(手机号3)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="ph3"
                                                          value="{{ $user->Phone2 }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                HOME NO(家庭电话)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="home_no"
                                                          value="{{ $user->tel }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                HOME ADDRESS(家庭地址)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="home_address"
                                                          value="{{ $user->Address }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                POSTAL CODE(邮编)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="code"
                                                          value="{{ $user->zip }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                CAMPANY NAME(企业名称)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="campany"
                                                          value="{{ $user->CompanyName }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                OFFICE NO(办公电话)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="office_no"
                                                          value="{{ $user->CompanyPhone }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                POSITION(职位)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="position"
                                                          value="{{ $user->Position }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                DATE OF SALARY(发薪日)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="salary_data"
                                                          value="{{ $user->Salary }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                LOE(工龄)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="loe"
                                                          value="{{ $user->WorkingAge }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                OFFICE ADDRESS(公司地址)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="office_add"
                                                          value="{{ $user->CompanyAddress }}"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                POSTAL CODE(邮编)
                            </label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="company_code"
                                                          value="{{ $user->CompanyZip }}"></div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                APPROVED ?(是否批款)
                            </label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="loan_cause">
                                    @foreach($loanStatus as $k=>$item)
                                        <option
                                            value="{{ $k }}" {{ $item==$user->LoanStatus?'selected':'' }}> {{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                REASON IF NO(若没批，注明原因)
                            </label>
                            <div class="col-sm-10">
                                <textarea name="loan_reason" id="" cols="30" rows="5"
                                          class="form-control">{{ $user->LoansCause }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right">
                                OTHER REMARKS(其他备注)
                            </label>
                            <div class="col-sm-10">
                                <textarea name="remarks" id="" cols="30" rows="5"
                                          class="form-control">{{ $user->Notes }}</textarea>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人1)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_1_name"
                                               value="{{ $user->rsname1 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_1_relation" value="{{ $user->rid1 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_1_ph"
                                               value="{{ $user->rsphone1 }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人2)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_2_name"
                                               value="{{ $user->rsname2 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_2_relation" value="{{ $user->rid2 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_2_ph"
                                               value="{{ $user->rsphone2 }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人3)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_3_name"
                                               value="{{ $user->rsname3 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_3_relation" value="{{ $user->rid3 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_3_ph"
                                               value="{{ $user->rsphone3 }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人4)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_4_name"
                                               value="{{ $user->rsname4 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_4_relation" value="{{ $user->rid4 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_4_ph"
                                               value="{{ $user->rsphone4 }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人5)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_5_name"
                                               value="{{ $user->rsname5 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_5_relation" value="{{ $user->rid5 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_5_ph"
                                               value="{{ $user->rsphone5 }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  row">
                            <label class="col-sm-2 col-form-label text-right">
                                CONTACT(联络人6)
                            </label>
                            <div class="col-sm-10">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="name" name="rela_6_name"
                                               value="{{ $user->rsname6 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="relationship"
                                               name="rela_6_relation" value="{{ $user->rid6 }}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="phone" name="rela_6_ph"
                                               value="{{ $user->rsphone6 }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> save</button>
                                <a href="/data" class="btn btn-white btn"> back </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

