@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/data"><strong>Data list 数据列表</strong></a>
                </li>
                <li class="breadcrumb-item active">
                    See details 查看详情
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    ID : {{ $user->uid }}
                    <div class="ibox-tools">
                        <a href="/data/{{ $user->uid }}/contract"> <span class="label label-primary">Download contract scan（下载合同扫描件） </span></a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover no-margins">
                        <thead>
                        <tr>
                            <th width="15%">属性名</th>
                            <th>属性值</th>
                            <th width="15%">属性名</th>
                            <th>属性值</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td> IC</td>
                            <td>{{ $user->IC }}</td>
                            <td> NAME1(名字)</td>
                            <td>{{ $user->rsname1 }}</td>
                        </tr>
                        <tr>
                            <td> COUNTER(分部)</td>
                            <td>{{ $user->department }}</td>
                            <td> RELATIONSHIP1(关系)</td>
                            <td>{{ $user->rid1 }}</td>
                        </tr>
                        <tr>
                            <td> Phone(手机)</td>
                            <td>
                                {{ $user->Phone }}
                                @if($user->Phone != ' ')
                                    <a href="/sms/send?ph={{ $user->Phone }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                            <td> HP1</td>
                            <td>{{ $user->rsphone1 }}
                                @if($user->rsphone1)
                                    <a href="/sms/send?ph={{ $user->rsphone1 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> Phone1</td>
                            <td>{{ $user->Phone1 }}
                                @if($user->Phone1)
                                    <a href="/sms/send?ph={{ $user->Phone1 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                            <td> NAME2(名字)</td>
                            <td>{{ $user->rsname2 }}</td>
                        </tr>
                        <tr>
                            <td> Phone2</td>
                            <td>{{ $user->Phone2 }}
                                @if($user->Phone2)
                                    <a href="/sms/send?ph={{ $user->Phone2 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                            <td> RELATIONSHIP2(关系)</td>
                            <td>{{ $user->rid2 }}</td>
                        </tr>
                        <tr>
                            <td> APPROVED ?(是否批款?)</td>
                            <td>{{ $user->LoanStatus }}</td>
                            <td> HP2</td>
                            <td>{{ $user->rsphone2 }}
                                @if($user->rsphone2 )
                                    <a href="/sms/send?ph={{ $user->rsphone2 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> REASON IF NO(原因)</td>
                            <td>{{ $user->LoansCause }}</td>
                            <td> NAME3(名字)</td>
                            <td>{{ $user->rsname3 }}</td>
                        </tr>
                        <tr>
                            <td> REMARKS(备注)</td>
                            <td>{{ $user->Notes }}</td>
                            <td> RELATIONSHIP3(关系)</td>
                            <td>{{ $user->rid3 }}</td>
                        </tr>
                        <tr>
                            <td> ADDRESS(住址)</td>
                            <td>{{ $user->Address }}</td>
                            <td> HP3</td>
                            <td>{{ $user->rsphone3 }}
                                @if($user->rsphone3)
                                    <a href="/sms/send?ph={{ $user->rsphone3 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> HOME NO(住家电话)</td>
                            <td>{{ $user->tel }}</td>
                            <td> NAME4(名字)</td>
                            <td>{{ $user->rsname4 }}</td>
                        </tr>
                        <tr>
                            <td> POSTAL CODE(住址邮编)</td>
                            <td>{{ $user->zip }}</td>
                            <td> RELATIONSHIP4(关系)</td>
                            <td>{{ $user->rid4 }}</td>
                        </tr>
                        <tr>
                            <td> CAMPANY NAME(公司名称)</td>
                            <td>{{ $user->CompanyName }}</td>
                            <td> HP4</td>
                            <td>{{ $user->rsphone4 }}
                                @if($user->rsphone4)
                                    <a href="/sms/send?ph={{ $user->rsphone4 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> POSITION(工作职位)</td>
                            <td>{{ $user->Position }}</td>
                            <td> NAME5(名字)</td>
                            <td>{{ $user->rsname5 }}</td>
                        </tr>
                        <tr>
                            <td> LOE(工龄)</td>
                            <td>{{ $user->WorkingAge }}</td>
                            <td> RELATIONSHIP5(关系)</td>
                            <td>{{ $user->rid5 }}</td>
                        </tr>
                        <tr>
                            <td> Date of Salary(发薪日)</td>
                            <td>{{ $user->Salary }}</td>
                            <td> HP5</td>
                            <td>{{ $user->rsphone5 }}
                                @if($user->rsphone5)
                                    <a href="/sms/send?ph={{ $user->rsphone5 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td> OFFICE NO(公司电话)</td>
                            <td>{{ $user->CompanyPhone }}</td>
                            <td> NAME6(名字)</td>
                            <td>{{ $user->rsname6 }}</td>
                        </tr>
                        <tr>
                            <td> ADDRESS(公司地址)</td>
                            <td>{{ $user->CompanyAddress }}</td>
                            <td> RELATIONSHIP6(关系)</td>
                            <td>{{ $user->rid6 }}</td>
                        </tr>
                        <tr>
                            <td> POSTAL CODE(公司邮编)</td>
                            <td>{{ $user->CompanyZip }}</td>
                            <td> HP6</td>
                            <td>{{ $user->rsphone6 }}
                                @if($user->rsphone6)
                                    <a href="/sms/send?ph={{ $user->rsphone6 }}"><i
                                            class="fa fa-envelope text-info"></i></a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

