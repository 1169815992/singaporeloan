<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header text-center">
                <a href="/account">
                    <img alt="image" class="rounded-circle" src="/img/profile_small.jpg">
                    <span class="block m-t-xs font-bold"> {{ auth()->user()->ename }} </span>
                </a>
            </li>

            <li class="{{ Request::is('/')?'active':''}}">
                <a href="/">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">Pandect 总览</span>
                </a>
            </li>

            <li class="{{ Request::is('data*')?'active':''}}">
                <a href="JavaScript:;">
                    <i class="fa fa-table"></i>
                    <span class="nav-label">Information 信息管理</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="true">
                    <li class="{{ Request::is('data/create')?'active':''}}">
                        <a href="/data/create">Type-in 信息录入 </a>
                    </li>
                    @if(Auth::user()->gid == '管理员')
                        <li class="{{ Request::is('data')?'active':''}}">
                            <a href="/data">List 信息列表 </a>
                        </li>
                    @endif
                </ul>
            </li>

            @if(Auth::user()->gid == '管理员' || Auth::user()->gid == '经理')
                <li class="{{ Request::is('staff*')?'active':''}}">
                    <a href="JavaScript:;">
                        <i class="fa fa-user"></i>
                        <span class="nav-label">Staff 员工管理</span> <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="true">
                        <li class="{{ Request::is('staff/division')?'active':''}}">
                            <a href="/staff/division">Branch 分部管理 </a>
                        </li>
                        <li class="{{ Request::is('staff')?'active':''}}">
                            <a href="/staff">List 员工列表 </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->gid == '管理员')
                <li class="{{ Request::is('os*')?'active':''}}">
                    <a href="JavaScript:;">
                        <i class="fa fa-cogs"></i>
                        <span class="nav-label">System 系统管理</span> <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="true">
                        <li class="{{ Request::is('os/role')?'active':''}}">
                            <a href="/os/role">Role 角色管理 </a>
                        </li>
                        <li class="{{ Request::is('os/log')?'active':''}}">
                            <a href="/os/log">Log 操作日志 </a>
                        </li>
                        {{--<li class="{{ Request::is('os/log')?'active':''}}">
                            <a href="/os/whole">Whole 全局配置 </a>
                        </li>--}}
                    </ul>
                </li>

                <li class="{{ Request::is('sms*')?'active':''}}">
                    <a href="JavaScript:;">
                        <i class="fa fa-envelope"></i>
                        <span class="nav-label"> SMS 短信管理</span> <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="true">
                        <li class="{{ Request::is('sms/send')?'active':''}}">
                            <a href="/sms/send">Send 发送短信 </a>
                        </li>
                        <li class="{{ Request::is('sms/sent')?'active':''}}">
                            <a href="/sms/sent">Sent 已发送 </a>
                        </li>
                        <li class="{{ Request::is('sms')?'active':''}}">
                            <a href="/sms"> reception 已接收 </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>
