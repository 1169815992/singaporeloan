<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> user management system 用户信息管理系统 </title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/laydate.js"></script>

    <script src="/js/wh.js"></script><!--外呼JS-->
</head>

<body class="pace-done">
<div id="wrapper">
    @include('nav')

    <div id="page-wrapper" class="gray-bg">
        <div class="row" style="margin-bottom: 10px">
            <nav class="navbar navbar-static-top" role="navigation" style="box-shadow: 2px 2px 2px #ccc">
                <div class="navbar-header">
                    <a class="minimalize-styl-2 btn btn-primary" href="/data/create">
                        <i class="fa fa-plus"></i> New 信息录入
                    </a>
                    <form class="navbar-form-custom" action="/data" method="get">
                        <div class="form-group">
                            <input type="text" placeholder="Search..." class="form-control" name="search"/>
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <button class="minimalize-styl-2 btn btn-info" onclick="signIn()">
                            <i class="fa fa-edit"></i> 坐席签入
                        </button>
                    </li>
                    <li>
                        <button class="minimalize-styl-2 btn btn-danger">
                            <i class="fa fa-level-up"></i> 挂断
                        </button>
                    </li>
                    <li>
                        <div style="height: 1px; width: 30px;"></div>
                    </li>
                    <li class="dropdown show">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="true">
                            坐席状态
                            <span class="label label-primary">忙线</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts" style="width: 130px">
                            <li><a href="javascript:;" class="dropdown-item"> 离线 </a></li>
                            <li><a href="javascript:;" class="dropdown-item"> 空闲 </a></li>
                            <li><a href="javascript:;" class="dropdown-item"> 被占用 </a></li>
                        </ul>
                    </li>
                    <li>
                        <div style="height: 1px; width: 50px;"></div>
                    </li>
                    <li>
                        <a href="/logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        @yield('content')
    </div>
    <script>
        var error = "{{ $errors->count() }}";
        if (error != 0) {
            alert('{{ $errors->first() }}');
        }
    </script>
</div>

<script src="/js/popper.min.js"></script> {{--弹框--}}
<script src="/js/bootstrap.js"></script>
<script src="/js/jquery.metisMenu.js"></script>
<script src="/js/jquery.slimscroll.min.js"></script>
<script src="/js/inspinia.js"></script>

@yield('js')

</body>
</html>
