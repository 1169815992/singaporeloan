@extends('basic')
@section('content')
    <div class="row white-bg">
        <div class=" col-md-12">
            <h1>快速导航</h1>
            <h4>
                <a href="/data/create">信息录入</a> &nbsp;&nbsp;
                @if(Auth::user()->gid == '管理员')
                    <a href="/data">信息查看</a> &nbsp;&nbsp;
                    <a href="https://cc.yunzongji.cn/" target="_blank">外呼系统</a>
                @endif
            </h4>
            <br>
        </div>
    </div>


    <div class="row">
        <div class=" col-md-12">
            <h1></h1>
            <div class="search-form white-bg" style="height: 96px;padding: 20px;">
                <form action="/data" method="get">
                    <div class="input-group">
                        <input type="text" placeholder="Search by IC or Phone or company name or Postal Code"
                               name="search"
                               class="form-control form-control-lg" style="height: 56px;border-color: #23c6c8"
                               autocomplete="off">
                        <input type="submit" class="btn btn-lg btn-info"
                               style="height: 56px;border-radius: 0;width: 10%" value="Search"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 40px;color: #ffffff">
        <div class="col-lg-3">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <h2 class="font-bold">{{ $userCount }}</h2>
                        <span> 用户总数 </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="widget style1 lazur-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-thumbs-up fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <h2 class="font-bold">{{ $userCountToday }}</h2>
                        <span> 今日新增 </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="widget style1 yellow-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-phone fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <h2 class="font-bold">{{ $staffCount }}</h2>
                        <span> 员工人数 </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="widget style1 red-bg">
                <div class="row">
                    <div class="col-4">
                        <i class="fa fa-cloud fa-5x"></i>
                    </div>
                    <div class="col-8 text-right">
                        <h2 class="font-bold">{{ $branchCount }}</h2>
                        <span> 分部数量 </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection
