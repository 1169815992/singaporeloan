@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data addition <small> 数据新增 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="/os/role"><strong>Role list 角色列表</strong></a>
                </li>
                <li class="breadcrumb-item">
                    home 新建角色
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-9 ibox">
                <div class="ibox-title">
                    <h5>Add role <small>添加角色</small></h5>
                </div>
                <div class="ibox-content">
                    <form action="/os/role/create" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 角色名 * </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="gname" required>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> save</button>
                                <a href="/os/log" class="btn btn-white btn"> cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
                <script>
                    var error = "{{ $errors->count() }}";
                    if (error != 0) {
                        alert('{{ $errors->first() }}');
                    }
                </script>
            </div>
        </div>
    </div>
@endsection

