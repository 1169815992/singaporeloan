@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Os management 角色管理</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-9 ibox">
                <div class="ibox-title">
                    <div class="col-sm-2">
                        <a href="/os/role/create" class="btn btn-info"> <i class="fa fa-plus"></i> 添加角色</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive text-center">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>name (角色名称)</th>
                                <th>status (状态)</th>
                                <th>Operation (操作)</th>
                            </tr>
                            @foreach($group as $item)
                                <tr>
                                    <td> {{ $item->gname }} </td>
                                    <td> {{ $item->gstatus }} </td>
                                    <td>
                                        <a href="/os/role/{{ $item->gid }}/edit" class="btn btn-white btn-sm"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

