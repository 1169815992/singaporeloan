@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Message list <small> 短信列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Message list 发送的短信</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <form action="/sms">
                        <div class="row">
                            <div class="col-sm-2">
                                <input type="text" name="phone" placeholder="来源号码" class="form-control">
                            </div>
                            <div class="col-sm-1">
                                <input type="number" name="port" placeholder="网关端口" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" id="start_time" name="start_time" class="form-control"
                                       autocomplete="off" placeholder="开始时间">
                                <script type="text/javascript">
                                    laydate.render({elem: '#start_time'});
                                </script>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" id="end_time" name="end_time" class="form-control" autocomplete="off"
                                       placeholder="结束时间">
                                <script type="text/javascript">
                                    laydate.render({elem: '#end_time'});
                                </script>
                            </div>
                            <div class="col-sm-1">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive text-center">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>time (时间)</th>
                                <th>source (来源号码)</th>
                                <th>content (短信内容)</th>
                                <th>reception (端口卡槽号码)</th>
                            </tr>
                            @if(!empty($content))
                                @foreach($content as $item)
                                    <tr>
                                        <td> {{ date('Y-m-d H:i:s',strtotime($item[0])) }} </td>
                                        <td> {{ $item[2] }} </td>
                                        <td> {{ $item[4] }} </td>
                                        <td> {{ $item[1] }} </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <p class="text-info">默认获取最近一周数据</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

