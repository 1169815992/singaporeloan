@extends('basic')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data list <small> 数据列表 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Operation log 操作日志</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-12 ibox">
                <div class="ibox-title">
                    <form action="/os/log">
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" id="product_name" name="path"
                                       placeholder="Search by path" class="form-control">
                            </div>

                            <div class="col-sm-3">
                                <input type="text" id="start_time" name="start_time" value="" class="form-control"
                                       autocomplete="off" placeholder="开始时间">
                                <script type="text/javascript">
                                    laydate.render({elem: '#start_time'});
                                </script>
                            </div>

                            <div class="col-sm-3">
                                <input type="text" id="end_time" name="end_time" value="{{ old('end_time') }}"
                                       class="form-control"
                                       autocomplete="off" placeholder="结束时间">
                                <script type="text/javascript">
                                    laydate.render({elem: '#end_time'});
                                </script>
                            </div>

                            <div class="col-sm-2">
                                <input type="submit" class="btn btn-info" value="搜索">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <style type="text/css">
                            .table-striped td {
                                max-width: 600px;
                                word-wrap: break-word;
                                word-break: break-all;
                                overflow: hidden;
                            }
                        </style>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>操作人</th>
                                <th>路径</th>
                                <th>方式</th>
                                <th>ip</th>
                                <th>信息</th>
                                <th>操作时间</th>

                            @foreach($operation as $item)
                                <tr>
                                    <td>{{ $item->uid }}</td>
                                    <td>{{ $item->path }}</td>
                                    <td>{{ $item->method }}</td>
                                    <td>{{ $item->ip }}</td>
                                    <td>{{ $item->input }}</td>
                                    <td>{{ $item->create }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {!! $operation->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

