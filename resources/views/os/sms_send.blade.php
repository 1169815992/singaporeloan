@extends('basic')

@section('content')
    <style>
        .form-inline .form-group {
            width: 33.3%;
        }

        .form-inline .form-group input {
            width: 100%;
        }
    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>send SMS <small> 发送短信 </small></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">home 首页</a>
                </li>
                <li class="breadcrumb-item">
                    <strong> Send SMS 发送短信 </strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="page-content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="col-md-9 ibox">
                <div class="ibox-title">
                    <h5>send SMS <small>发送短信</small></h5>
                </div>
                <div class="ibox-content">
                    <form action="/sms/send" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 短信内容 * </label>
                            <div class="col-sm-10">
                                <textarea name="content" class="form-control" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-right"> 发送号码 * </label>
                            <div class="col-sm-10">
                                <textarea name="phone" class="form-control" rows="4" placeholder="多个号码用,隔开">{{ $ph }}</textarea>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary btn" type="submit"> send</button>
                                <a href="/os/log" class="btn btn-white btn"> cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
                <script>
                    var error = "{{ $errors->count() }}";
                    if (error != 0) {
                        alert('{{ $errors->first() }}');
                    }
                </script>
            </div>
        </div>
    </div>
@endsection

